//
//  CCCLibraryApi.swift
//  CCCFramework
//
//  Created by Viggnesh K on 3/10/15.
//  Copyright (c) 2015 wsm. All rights reserved.
//

import UIKit

public class CCCLibraryApi: NSObject
{
    
    public func LibrarySingleton1(fileName: String) -> String
    {
        return "Hellow boss, this is awesome \(fileName)"
    }

    
    public func LibrarySingleton2(fileName: String) -> String
    {
        return "Hellow boss, this is awesome \(fileName)"
    }

    
    //1
    class var sharedInstance: CCCLibraryApi {
        //2
        struct Singleton {
            //3
            static let instance = CCCLibraryApi()
        }
        //4
        return Singleton.instance
    }
    
    override init() {
        super.init()
    }

    
}