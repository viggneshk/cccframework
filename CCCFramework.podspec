Pod::Spec.new do |s|
  s.name = 'CCCFramework'
  s.version = '0.0.1'
  s.license = 'MIT'
  s.summary = 'CCCFramework Made insanely simple'
  s.homepage = 'https://viggneshk@bitbucket.org/viggneshk/cccframework.git'
  s.social_media_url = 'https://twitter.com/viggneshk'
  s.authors = { 'Viggnesh Kandasamy' => 'viggneshk@gmail.com' }
  s.source = { :git => 'https://viggneshk@bitbucket.org/viggneshk/cccframework.git', :tag => s.version }

  s.ios.deployment_target = '8.0'

  s.source_files = 'CCCFramework/*.h'

  s.requires_arc = true
end
