#!/bin/sh
################################################################################
#
# Version 1.0 , March 2015
#
# Purpose:
#   Create a Universal Cocoa Touch library for Simulators and Devices for testing aka Debug!
#   If you don&#8217;t know AppStore has changed rules. Fat/Universal binaries(binary with Simulator(i386,x86_64)
#   and Device(armv7,armv7s,arm64) are no longer passing validation!. Your app must contain framework with only &#8220;Release&#8221; Device arhitectures.
#
# Author: Ziga Drole - www.zigadrole.com , https://twitter.com/zig3c
#################################################################################


set -e
set -o pipefail

#This script only executes for Debug configuration. For release we dont need universal. read above!!
#if [ ${CONFIGURATION} = "Debug" ]
#then

echo "############################################# XCODE INVOCATION #################################################"
# To understand: making build for simulator calls this script which makes device build!. because this device build calls again this script we create recursion!
#  So this is howwe stop recursion with ALREADYINVOKED.
if [ "true" == ${ALREADYINVOKED:-false} ]
then
echo "RECURSION: I am NOT the root invocation, so I'm NOT going to recurse"
else

# Prevent infinite recursion (Xcode)
export ALREADYINVOKED="true"

echo "######## COMMAND TO GET ALL VARIABLES,BUILD SETTINGS like SDK_VERSION,CONFIGURATION,EXECUTABLE_NAME ###########"
echo "terminal: xcodebuild -project CCCFramework.xcodeproj -target CCCFramework -showBuildSettings"
#xcodebuild -project "${PROJECT_NAME}.xcodeproj" -target "${PROJECT_NAME}" -showBuildSettings"
echo "###############################################################################################################"


# First, find and set BASESDK version number
SDK_VERSION=$(echo ${SDK_NAME} | grep -o '.\{3\}$')

# Next, work out if we're in SIM or DEVICE

if [ ${PLATFORM_NAME} = "iphonesimulator" ]
then
OTHER_SDK_TO_BUILD=iphoneos${SDK_VERSION}
else
OTHER_SDK_TO_BUILD=iphonesimulator${SDK_VERSION}
fi
echo "################################# XCODE SELECTED SDK, SCRIPT CREATES OTHER ###################################"
echo "XCODE HAS SELECTED PLATFORM: ${PLATFORM_NAME} , VERSION: ${SDK_VERSION} (iOS Deployment Target: ${IPHONEOS_DEPLOYMENT_TARGET})"
echo "SCRIPT CREATING BUILD FOR, OTHER_SDK_TO_BUILD = ${OTHER_SDK_TO_BUILD}"

echo "SUCCEED: creating missing build"

echo "BUILDING: invoking: xcodebuild -configuration \"${CONFIGURATION}\" -project \"${PROJECT_NAME}.xcodeproj\" -target \"${TARGET_NAME}\" -sdk \"${OTHER_SDK_TO_BUILD}\" RUN_CLANG_STATIC_ANALYZER=NO ONLY_ACTIVE_ARCH=NO BUILD_DIR=\"${BUILD_DIR}\" BUILD_ROOT=\"${BUILD_ROOT}\" clean build"
xcodebuild -configuration "${CONFIGURATION}" -project "${PROJECT_NAME}.xcodeproj" -target "${PROJECT_NAME}" -sdk "${OTHER_SDK_TO_BUILD}" RUN_CLANG_STATIC_ANALYZER=NO ONLY_ACTIVE_ARCH=NO BUILD_DIR="${BUILD_DIR}" BUILD_ROOT="${BUILD_ROOT}" clean build &>/dev/null || die "xcodebuild workspace command failed!!"

#Merge all binaries as a fat binary

# Current paths for debug-iphoneos and debug-iphonesimulator
CURRENTCONFIG_DEVICE_DIR=${SYMROOT}/${CONFIGURATION}-iphoneos
CURRENTCONFIG_SIMULATOR_DIR=${SYMROOT}/${CONFIGURATION}-iphonesimulator

echo "################################# XCODE TAKING BUILDS FROM PATH #############################################"
echo "Taking device build from: ${CURRENTCONFIG_DEVICE_DIR}"
echo "Taking simulator build from: ${CURRENTCONFIG_SIMULATOR_DIR}"

echo "################################# XCODE LIPO,CREATING UNIVERSAL #############################################"

CREATING_UNIVERSAL_DIR=${SYMROOT}/${CONFIGURATION}-universal

if [ -d "${CREATING_UNIVERSAL_DIR}" ]
then
echo "UNIVERSAL directory: alredy created, deleting everything from ${CREATING_UNIVERSAL_DIR}"
rm -rf "${CREATING_UNIVERSAL_DIR}/"
fi
echo "UNIVERSAL directory: creating fresh folder: ${CREATING_UNIVERSAL_DIR}"
mkdir -p "${CREATING_UNIVERSAL_DIR}"

echo "UNIVERSAL directory: Copying everything from iphonesimulator to universal including swift modules"
cp -r "${CURRENTCONFIG_SIMULATOR_DIR}/${EXECUTABLE_NAME}.framework" "${CREATING_UNIVERSAL_DIR}"
echo "UNIVERSAL directory: Copying swift modules from iphoneos to universal"
cp -r "${CURRENTCONFIG_DEVICE_DIR}/${EXECUTABLE_NAME}.framework/Modules/${EXECUTABLE_NAME}.swiftmodule/" "${CREATING_UNIVERSAL_DIR}/${EXECUTABLE_NAME}.framework/Modules/${EXECUTABLE_NAME}.swiftmodule"

echo "UNIVERSAL: LIPO :current configuration (${CONFIGURATION})"
echo "UNIVERSAL: LIPO :output file at path: ${CREATING_UNIVERSAL_DIR}/${EXECUTABLE_NAME}.framework/${EXECUTABLE_NAME}"
lipo -create "${CURRENTCONFIG_DEVICE_DIR}/${EXECUTABLE_NAME}.framework/${EXECUTABLE_NAME}" "${CURRENTCONFIG_SIMULATOR_DIR}/${EXECUTABLE_NAME}.framework/${EXECUTABLE_NAME}" -output "${CREATING_UNIVERSAL_DIR}/${EXECUTABLE_NAME}.framework/${EXECUTABLE_NAME}"
#xcrun -sdk iphoneos lipo...

echo "################################ NOTE ABOUT ARHITECTURES  #################################################"
echo "Current arhitectures:suported devices"
echo "x86_64 :Simulator,PCs,32&64 bit processors."
echo "i386   :Simulator,PCs,32 bit processors."
echo "ARMv8 / ARM64  == (16777228) cpusubtype (0):  ==>  iPhone6, iPhone6+, iPhone 5s, iPad Air, Retina iPad Mini"
echo "ARMv7s == cputype (12) cpusubtype (11)  ==> iPhone 5, iPhone 5c, iPad 4"
echo "ARMv7  ==> iPhone 3GS, iPhone 4, iPhone 4S, iPod 3G/4G/5G, iPad, iPad 2, iPad 3, iPad Mini"
echo "ARMv6  ==> iPhone, iPhone 3G, iPod 1G/2G "
echo "#########################################################################################################"
echo "Library file at path: ${CREATING_UNIVERSAL_DIR}/${EXECUTABLE_NAME}"
echo "Included arhitectures:"
file ${CREATING_UNIVERSAL_DIR}/${EXECUTABLE_NAME}.framework/${EXECUTABLE_NAME}

fi
#fi
