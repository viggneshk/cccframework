
##Precautions
For performance issues, it is suggested that you should use only one instance of CMMotionManager throughout the app. Make sure to stop receiving updates from the sensors as soon as you get your work done.
You can do this in MotionKit like this.
```swift

    //Make sure to call the required function when you're done
    motionKit.stopAccelerometerUpdates()
    motionKit.stopGyroUpdates()
    motionKit.stopDeviceMotionUpdates()
    motionKit.stopmagnetometerUpdates()

```

##Delegates
In case if you dont want to use the trailing closures, we've got you covered. MotionKit supports the following Delegate methods to retrieve the sensor values.
```swift
    optional func retrieveAccelerometerValues (x: Double, y:Double, z:Double, absoluteValue: Double)
    optional func retrieveGyroscopeValues     (x: Double, y:Double, z:Double, absoluteValue: Double)
    optional func retrieveDeviceMotionObject  (deviceMotion: CMDeviceMotion)
    optional func retrieveMagnetometerValues  (x: Double, y:Double, z:Double, absoluteValue: Double)


    optional func getAccelerationValFromDeviceMotion        (x: Double, y:Double, z:Double)
    optional func getGravityAccelerationValFromDeviceMotion (x: Double, y:Double, z:Double)
    optional func getRotationRateFromDeviceMotion           (x: Double, y:Double, z:Double)
    optional func getMagneticFieldFromDeviceMotion          (x: Double, y:Double, z:Double)
    optional func getAttitudeFromDeviceMotion               (attitude: CMAttitude)

```
To use the above delegate methods, you have to add the MotionKit delegate to your ViewController.
```swift
    class ViewController: UIViewController, MotionKitDelegate {
      ...
    }
```
And in the ViewDidLoad method, you simply have to add this.
```swift
    override func viewDidLoad() {
        super.viewDidLoad()
        motionKit.delegate = self
        ......
      }

```
Having that done, you'd probably want to implement a delegate method like this.
```swift
    func retrieveAccelerometerValues (x: Double, y:Double, z:Double, absoluteValue: Double){
      //Do whatever you want with the x, y and z values. The absolute value is calculated through vector mathematics
      ......
    }

   func retrieveGyroscopeValues (x: Double, y:Double, z:Double, absoluteValue: Double){
    //Do whatever you want with the x, y and z values. The absolute value is calculated through vector mathematics
    ......
   }

```

##Getting just a single value at an instant
if you want to get just a single value of any of the available sensors at a given time, you could probably use some of the our handy methods provided in MotionKit.

###Accelerometer

<div align="right">
<h7><i>Swift</i></h7>
</div>
```swift
    motionKit.getAccelerationAtCurrentInstant {
        (x, y, z) -> () in
        ....
      }
```

<div align="right">
<h7><i>Objective-C</i></h7>
</div>
```objective-c
    [motionKit getAccelerationAtCurrentInstant:1.0 values:^(double x, double y,double z) {
    // your values here
    }];
```

###Gravitational Acceleration

<div align="right">
<h7><i>Swift</i></h7>
</div>
```swift
    motionKit.getAccelerationAtCurrentInstant {
      (x, y, z) -> () in
      ....
        }
```

<div align="right">
<h7><i>Objective-C</i></h7>
</div>
```objective-c
    [motionKit getAccelerationAtCurrentInstant:1.0 values:^(double x, double y,double z) {
    // your values here
    }];
```

###Attitude

<div align="right">
<h7><i>Swift</i></h7>
</div>
```swift
    motionKit.getAttitudeAtCurrentInstant {
      (x, y, z) -> () in
      ....
        }
```

<div align="right">
<h7><i>Objective-C</i></h7>
</div>
```objective-c
    [motionKit getAttitudeAtCurrentInstant:1.0 values:^(double x, double y,double z) {
    // your values here
    }];
```

###Magnetic Field

<div align="right">
<h7><i>Swift</i></h7>
</div>
```swift
    motionKit.getMageticFieldAtCurrentInstant {
      (x, y, z) -> () in
      ....
        }
```

<div align="right">
<h7><i>Objective-C</i></h7>
</div>
```objective-c
    [motionKit getMageticFieldAtCurrentInstant:1.0 values:^(double x, double y,double z) {
    // your values here
    }];
```

###Gyroscope Values

<div align="right">
<h7><i>Swift</i></h7>
</div>
```swift
    motionKit.getGyroValuesAtCurrentInstant {
      (x, y, z) -> () in
      ....
        }
```

<div align="right">
<h7><i>Objective-C</i></h7>
</div>
```objective-c
    [motionKit getGyroValuesAtCurrentInstant:1.0 values:^(double x, double y,double z) {
    // your values here
    }];
```



#Discussion
- You can join our [Reddit] (https://www.reddit.com/r/MotionKit/) channel to discuss anything.

- You can also open an issue here for any kind of feature set that you want. We would love to hear from you.

- Don't forget to subscribe our Reddit channel, which is [/r/MotionKit] (https://www.reddit.com/r/MotionKit/)

- Our StackOverflow tag is 'MotionKit'


#Requirements
* iOS 7.0+
* Xcode 6.1

#TODO
- [ ] Add More Methods
- [ ] Adding Background Functionality

#License
<a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">Creative Commons Attribution-NonCommercial 4.0 International License</a>.
